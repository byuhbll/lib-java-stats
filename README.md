Tools for collecting and reporting statistical information.

# Getting Started

Add the following dependency to your Maven POM. 

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>stats</artifactId>
  <version>1.0.1</version>
</dependency>
```

# Times

A collection of tools that gathers benchmarking, occurrence, and timing statistics for processes and subcomponents of processes. The Times class is your starting point. There are different types of statistics that Times supports: Benchmark, History, PerfectHistory. After constructing a new Times with a particular statistical type, start adding occurrences of processes with their accompanying durations by calling mark() or using a Times.Marker object. Break down process stats into smaller components by specifying a label for different spots (ie, mark(Object) or Times.Marker.mark(Object)). Times.Marker should be used if duration statistics are required.

Unless otherwise noted as with `Marker`, the classes in edu.byu.hbll.stats.time are thread safe. All `Statistic` classes are designed to be added to over and over again and by multiple threads. Though `Statistic`s are generally used within the context of the `Times` class, they may also be used standalone.

Example 1:

```java
Marker m = Times.single();
// first component of the process
m.mark(1);
// second component of the process
m.mark(2);
// third component of the process
m.mark(3);
System.out.println(m);
```

Example 2:

```java
Times<Benchmark> t = Times.benchmark();
Marker m = t.marker();
// first component of the process
m.mark(1);
// second component of the process
m.mark(2);
// third component of the process
m.mark(3);
System.out.println(t);
```
 
Example 3:

```java
private Times<History> t = new Times<>(new History(ChronoUnit.MINUTES, 120));
 
public void run() {
    Marker m = t.marker();
    // first component of the process
    m.mark(1);
    // second component of the process
    m.mark(2);
    // third component of the process
    m.mark(3);
}
```

Example 4:

```java
private static Times<PerfectHistory> t = Times.perfectHistory();
 
public void run() {
    // run some process
    t.mark();
}
```
